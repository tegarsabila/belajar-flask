from flask import jsonify, Blueprint, abort, request
from flask_restful import Resource, Api, reqparse, fields, marshal, marshal_with
import time

name_fields = {
    'name':fields.String,
    'timestamp':fields.Integer,
    'character_count':fields.Integer
}

class Inputname(Resource):
    def get(self):
        text = str(request.args.get('input'))
        character_count = len(text)
        data_set = {'name':text,'timestamp':time.time(),'character_count':character_count}
        return marshal(data_set, name_fields)
        

tools_api = Blueprint('resourcestools', __name__)
api       = Api(tools_api)
api.add_resource(Inputname, '/name', endpoint='name')