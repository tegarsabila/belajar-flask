from flask import Flask, request
from flask_restful import Resource, Api
from resources.tools import tools_api

app = Flask(__name__)
api = Api(app)

app.register_blueprint(tools_api, url_prefix='/api/v1')

if __name__ == '__main__':
    app.run(debug=True)